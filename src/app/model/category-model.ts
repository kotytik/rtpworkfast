export class CategoryModel {
    key: string;
    name: string;           // название
    description: string;    // описание категории
}
